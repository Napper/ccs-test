# CCS Test

Investigation into using pipelines for a CCS project.

* The "out of the box" project for the [SimpleLink Ethernet MSP432E401Y MCU Launchpad Development Kit](https://www.ti.com/tool/MSP-EXP432E401Y) was imported to CCS following [Quick Start Guide](https://software-dl.ti.com/simplelink/esd/simplelink_msp432e4_sdk/1.55.00.21/docs/simplelink_mcu_sdk/Quick_Start_Guide.html) and then committed and pushed to GitLab.
* A Docker image was created for the pipeline using the following Dockerfile. This is rather large at present, probably because it is based on Ubuntu but this was used as the base because it is [supported by CCS](https://software-dl.ti.com/ccs/esd/documents/ccsv10_linux_host_support.html).
```
# Use a base supported by CCS: https://software-dl.ti.com/ccs/esd/documents/ccsv10_linux_host_support.html
FROM ubuntu:20.04 AS builder1

# Add items identified as required in https://software-dl.ti.com/ccs/esd/documents/ccsv10_linux_host_support.html
RUN apt-get update && apt-get install -y libc6-i386 libusb-0.1-4 libgconf-2-4 libncurses5 libpython2.7 libtinfo5

# Add SimpleLink MSP432 Software Development Kit: https://www.ti.com/tool/SIMPLELINK-MSP432-SDK
FROM builder1 AS addSimpleLink
COPY ./simplelink_msp432e4_sdk_4_20_00_12.run .
RUN chmod +x simplelink_msp432e4_sdk_4_20_00_12.run && ./simplelink_msp432e4_sdk_4_20_00_12.run --unattendedmodeui none --mode unattended

# Copy only what we want to avoid bloating the image
FROM builder1 AS builder2
COPY --from=addSimpleLink /opt/ti /opt/ti

# Add CCS: https://software-dl.ti.com/ccs/esd/documents/ccs_downloads.html
# using command line: https://software-dl.ti.com/ccs/esd/documents/ccs_installer-cli.html
FROM builder2 AS addCCS
COPY ./CCS10.4.0.00006_linux-x64.tar.gz .
RUN tar xf CCS10.4.0.00006_linux-x64.tar.gz
WORKDIR $CWD/CCS10.4.0.00006_linux-x64
RUN chmod +x ccs_setup_10.4.0.00006.run && ./ccs_setup_10.4.0.00006.run --unattendedmodeui none --mode unattended --enable-components "PF_MSP432"

# Copy only what we want to avoid bloating the image
FROM builder2 AS builder3
COPY --from=addCCS /root/ti /root/ti

# Configure CSS with location of SimpleLink using commands https://software-dl.ti.com/ccs/esd/documents/ccs_projects-command-line.html
RUN /root/ti/ccs1040/ccs/eclipse/eclipse -nosplash -data . -application com.ti.common.core.initialize -ccs.productDiscoveryPath "/opt/ti/simplelink_msp432e4_sdk_4_20_00_12"
```
